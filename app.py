#! /usr/bin/env python3
from flask import Flask
import os.path
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.script import Manager
from flask.ext.bootstrap import Bootstrap
def mkpath(p):
	return os.path.normpath(
		os.path.join(
			os.path.dirname(__file__),
			p))
app = Flask(__name__)
app.debug = True
manager = Manager(app)
app.config['BOOTSTRAP_SERVE_LOCAL'] = True
Bootstrap(app)
db = SQLAlchemy(app)
from .models import Album,Genre
app.config['SQLALCHEMY_DATABASE_URI'] = (
	'sqlite:///'+mkpath('albums.db'))
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"]=False
db.create_all()
db.session.commit()
