from .app import app
from flask import render_template, request
from .models import *
images_dir = "/static/images/"
genres = []
genres_keys = []
premiersChar = []
@app.route("/")
def home():
	return render_template("home.html",title="Bienvenue sur notre site")

@app.route("/musiques")
def musiques():
	global genres
	global genres_keys
	global premiersChar
	if genres==[]:
		genres=getGenres()
		genres_keys = sorted(genres)
		premiersChar = getPremiersChar()
		print("okkkkkkkkkkkkkkkk");
	return render_template("musiques.html",premiersChar=premiersChar, images=getRandomImages(), genres=genres, genres_keys=genres_keys,
		images_dir=images_dir)
@app.route("/music/<premiereLettre>/<page>")
def music(premiereLettre, page):
	page = int(page)
	if len(premiereLettre) > 1:
		premiereLettre=premiereLettre[0]
	a = getDixArtistes(premiereLettre, page)
	print("ok")
	print(getAlbumsDixArtistes(premiereLettre, page))
	print("ok")
	return render_template("music.html", artistes=a[0], 
		titre=premiereLettre.upper(), albums=getAlbumsDixArtistes(premiereLettre, page), 
		nextpage=a[1], page_suivante=page+1, precedente=getPrecedente(page), max_pages=getNombrePages(premiereLettre),
		dirNextPage="music", images_dir=images_dir)

@app.route("/musics/<genre>/<page>")
def musics(genre, page):
	page = int(page)
	a = getDixArtistesGenre(genre, page)
	return render_template("music.html", artistes=a[0],
		titre=genre, albums=getAlbumsDixArtistesGenre(genre, page),
		nextpage=a[1], page_suivante=page+1, precedente=getPrecedente(page), max_pages=getNombrePagesGenre(genre),
		dirNextPage="musics", images_dir=images_dir)

@app.route("/about")
def about():
	return render_template("about.html")

@app.route("/search")
def search():
	return render_template("search.html")

@app.route("/search", methods=['GET','POST'])
def recherche():
	motscles = request.form["motscles"]
	pasvide = False
	for car in motscles:
		if car != " ":
			pasvide = True
	if request.method == 'POST' and motscles != "" and pasvide:
		checked = request.form.getlist('1')
		titre = True if "Titre" in checked else False
		artiste = True if "Artiste" in checked else False
		genre = True if "Genre" in checked else False
		annee = True if "Annee" in checked else False
		recherche = rechercher(motscles,titre,artiste,genre,annee)
		artistes = []
		for img,title,by in recherche:
			if by not in artistes:
				artistes.append(by)
		return render_template("music.html",artistes=artistes,
			titre=motscles, albums=recherche,
			nextpage=False, page_suivante=1, precedente=1, max_pages=1,
			dirNextPage="",images_dir=images_dir)
	return render_template("search.html")

@app.route("/musiques/<name>/<img>")
def description(name, img):
	album = getAlbumByNameAndImg(name, img)[0]
	genresL = [g[0] for g in getGenresByNameAndImg(name, img)]
	genres=""
	for i in range(len(genresL)):
		if i != len(genresL)-1:
			genres+=genresL[i]+", "
		else:
			genres+=genresL[i]+"."
	return render_template("description.html", title=name, groupe=album[0], releaseYear=album[4], genres=genres, album=(img,name), images_dir=images_dir)